package ft;

public class SecurityInfo {

    private String cardNumber;
    private String empNumber;
    
    private String createDateStr;
    private String expiryDateStr;
    private String staffName;
    private String groupName;
    
    public SecurityInfo(String cardNumber, String empNumber, String createDateStr, 
    			String expiryDateStr, String staffName, String groupName) {
    	
    	this.cardNumber = cardNumber;
    	this.empNumber = empNumber;
    	this.createDateStr = createDateStr;
    	this.expiryDateStr = expiryDateStr;
    	this.staffName = staffName;
    	this.groupName = groupName;
    	
    }
    
	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public String getEmpNumber() {
		return empNumber;
	}
	
	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}
	
	public String getCreateDateStr() {
		return createDateStr;
	}
	
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	
	public String getExpiryDateStr() {
		return expiryDateStr;
	}
	
	public void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}
	
	public String getStaffName() {
		return staffName;
	}
	
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
}
