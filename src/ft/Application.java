package ft;

import ft.common.controller.DatabaseController;
import ft.common.controller.FileController;
import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import ft.SecurityInfo;

 public class Application
 {
	  private static Log FTLogger = LogFactory.getLog(Application.class);
	  
	  public Application()
	  {
	    URL url = getClass().getResource("/log4j.properties");
	    PropertyConfigurator.configure(url);
	  }
	  
	  public static void main(String[] args)
	  {
	    Application app = new Application();
	    FTLogger.info("Security Application Started");
	    File file = new File("./Configuration.properties");
	    
	    if (file.exists() == false) {
	    	FTLogger.error("Configuration file not found. Please place the Configuration.properties file on the same path as the Security.jar file.");
	    	return;
	    }
	    else {
	    	FTLogger.info("Configuration file path: " + file.getAbsolutePath());
	    }
	    
	    FTLogger.info("Security Application Started...");
	    
	    app.executeTask();
	    
	    FTLogger.info("Security Application Completed!");
	  }
	  
	  private void cleanUpRecords( ) {
		  
	    Date curDate = new Date();
	    FTLogger.info("========================================");
	    FTLogger.info("Removing Expired Access Cards...");
	    FTLogger.info("Executution Date : " + curDate);
	    
	    int cnt = DatabaseController.getInstance().cleanUpSecurityInfo(curDate);
	    FTLogger.info("Record[s] removed : " + cnt);
	    FTLogger.info("========================================");
	    
	  }
	  
	  
	  private boolean isFound(HashSet source, String filename) {

		  Iterator<String> iterFilePrefix = source.iterator();
	      
		  if ((source != null) && (source.size() > 0)) {
			  while (iterFilePrefix.hasNext())
		      {
		        String prefix = (String)iterFilePrefix.next();
		        if (filename.startsWith(prefix))
		        {
		          return true;
		        }
		      }
		  }
	      
	      return false;
	  }
	  
	  private void updateRecords() {
		  
		    FileController.initFileController();
		    File[] files = FileController.getFileList(FileController.getSourceDirectory());
		    HashSet<String> filePrefixes = FileController.getFilePrefixList();
		    HashSet<String> ignoreList = FileController.getIgnoreList();
		    
		    if (files.length == 0) {
		      FTLogger.info("There is no file to process!");
		      return;
		    }
		    
		    for (int i = 0; i < files.length; i++)
		    {

		      String fName = files[i].getName();

		      if (isFound(filePrefixes, fName) == false)
		      {
		        FTLogger.info("========================================");
		        FTLogger.info(fName + " is not a valid file!");
		        FTLogger.info("========================================");
		        FileController.moveFile(files[i].getName(), true);
		      }
		      else
		      {
		    	FTLogger.info("Reading File..");  
		        List<String> fileDetails = FileController.readFromFile(files[i]);
		        FTLogger.info("Reading File Completed. ");
		        
		        if ((fileDetails != null) && (fileDetails.size() > 0))
		        {
		          FTLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		          FTLogger.info("Processing for " + fName + " started at : " + new Date());
		          FTLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		          
		          int ctr = 0, maxCount = 0;
		          Iterator<String> iterFileDetails = fileDetails.iterator();
		          
		          maxCount = fileDetails.size();
		          
		          while (iterFileDetails.hasNext())
		          {
		            String row = null;
		            try
		            {
		              ctr++;
		              
		              FTLogger.info("Processing " + ctr + " of " + maxCount + "...");    
		              
		              row = (String)iterFileDetails.next();
		              
		              if ((row != null) && (row.trim().length() > 0))
		              {
		                String[] details = row.split(FileController.getDelimiter());
		                
		                SecurityInfo securityInfo = new SecurityInfo(details[0], details[1], details[3],
		                		details[4], details[5], details[6]);
		                
		                
		                if (isFound(ignoreList, securityInfo.getGroupName()) == true)
		                  {
		                    FTLogger.warn("Skipping row for " + securityInfo.getStaffName() + 
		                    				" with group " + securityInfo.getGroupName() + "...");
		                    
		                    continue;
		                  }
		                
		                DatabaseController.getInstance().updateSecurityInfo(securityInfo);
		                
		              }
		              
		            }
		            catch (Exception e)
		            {
		              FTLogger.error("Invalid row data. row no: " + ctr + " data :" + row);
		            }
		            
		          }
		          
		          FileController.moveFile(files[i].getName());
		          
		          FTLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		          FTLogger.info("Processing for " + files[i].getName() + " completed at : " + 
		            new Date());
		          FTLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		        }
		      }
		    }
		    
		    DatabaseController.getInstance().closeConnection();
		    FTLogger.info("Task Completed!");
		
	  }
	  
	  
	  public void executeTask()
	  {
	    updateRecords();
	    cleanUpRecords();
	  }
	  
 }


