package ft.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileController
{
  private static Log FTLogger = LogFactory.getLog(FileController.class);
  private static String SOURCE_DIR = null;
  private static String PROCESSED_DIR = null;
  private static String ERROR_DIR = null;
  private static String DELIMITER = null;
  private static HashSet<String> FILE_PREFIX_LIST = null;
  private static HashSet<String> IGNORE_LIST = null;
  
  private static FilenameFilter fileFilter = new FilenameFilter()
  {
    public boolean accept(File dir, String name)
    {
      String fileExt = name.substring(name.lastIndexOf("."));
      if (fileExt != null) {
        if (fileExt.equalsIgnoreCase(Configuration.getString("fileExt"))) {
          return true;
        }
      }
      return false;
    }
  };
  
  public static String getDelimiter()
  {
    return DELIMITER;
  }
  
  public static String getErrorDirectory()
  {
    return ERROR_DIR;
  }
  
  public static File[] getFileList(String dirStr)
  {
    File dir = new File(dirStr);
    
    return dir.listFiles(fileFilter);
  }
  
  public static HashSet<String> getFilePrefixList()
  {
    return FILE_PREFIX_LIST;
  }
  
  public static HashSet<String> getIgnoreList()
  {
    return IGNORE_LIST;
  }
  
  public static String getProcessedDirectory()
  {
    return PROCESSED_DIR;
  }
  
  public static String getSourceDirectory()
  {
    return SOURCE_DIR;
  }
  
  public static void initFileController()
  {
    FTLogger.info("Initializing File Directories...");
    
    SOURCE_DIR = Configuration.getString("srcDir");
    PROCESSED_DIR = Configuration.getString("processedDir");
    ERROR_DIR = Configuration.getString("errorDir");
    DELIMITER = Configuration.getString("delimiter");
    
    String prefixes = Configuration.getString("filePrefix");
    String[] prefixGrp = prefixes.split(";");
    if (prefixGrp.length > 0) {
      FILE_PREFIX_LIST = new HashSet(Arrays.asList(prefixGrp));
    }
    
    String ignoreGrpStr = Configuration.getString("IgnoreGroup");
    String[] ignoreGrp = ignoreGrpStr.split(";");
    if (ignoreGrp.length > 0) {
      IGNORE_LIST = new HashSet(Arrays.asList(ignoreGrp));
    }
    
    File sourceDir = new File(SOURCE_DIR);
    if (!sourceDir.exists())
    {
      FTLogger.info("Creating Source Directory...");
      sourceDir.mkdirs();
    }
    
    File processedDir = new File(PROCESSED_DIR);
    if (!processedDir.exists())
    {
      FTLogger.info("Creating Processed Directory...");
      processedDir.mkdirs();
    }
    
    File errorDir = new File(ERROR_DIR);
    if (!errorDir.exists())
    {
      FTLogger.info("Creating Error Directory...");
      errorDir.mkdirs();
    }
    
    FTLogger.info("Initialization Complete!");
  }
  
  public static void moveFile(String fname)
  {
    moveFile(fname, false);
  }
  
  public static void moveFile(String fname, boolean isError)
  {
    File srcFile = new File(SOURCE_DIR + "\\" + fname);
    
    Date curDate = new Date();
    String name = fname.substring(0, fname.indexOf("."));
    String ext = fname.substring(fname.indexOf(".") + 1);
    
    StringBuilder builder = new StringBuilder();
    
    if (isError)
    {
      builder.append(ERROR_DIR);
      FTLogger.info("Moving " + fname + " to " + ERROR_DIR + "...");
    }
    else
    {
      builder.append(PROCESSED_DIR);
      FTLogger.info("Moving " + fname + " to " + PROCESSED_DIR + "...");
    }
    
    builder.append(System.getProperty("file.separator")).append(name).append("_").append(curDate.getTime()).append(".").append(ext);
    
    File destFile = new File(builder.toString());
    if (srcFile.renameTo(destFile)) {
      FTLogger.info("File has been moved successfully!");
    } else {
      FTLogger.error("Unable to rename/move " + fname);
    }
  }
  
  public static List<String> readFromFile(File file)
  {
    List<String> details = null;
    Scanner scanner = null;
    try
    {
      if ((file == null) || (!file.exists()))
      {
        FTLogger.error("File does not Exist!");
        
        List<String> localList1 = details;return localList1;
      }
      int skipRow = Configuration.getValue("skippedRowCnt");
      int ignoreRow = Configuration.getValue("ignoreRowCnt");
      int ctr = 0;
      details = new ArrayList();
      FTLogger.info("Parsing file " + file.getName() + "...");
      scanner = new Scanner(new FileInputStream(file));
      while (scanner.hasNextLine())
      {
        String row = scanner.nextLine();
        if (ctr != skipRow)
        {
          ctr++;
          FTLogger.info("Skipping Row " + ctr);
        }
        else
        {
          details.add(row);
        }
      }
      for (int i = 0; i < ignoreRow; i++)
      {
        int lastRow = details.size() - 1;
        FTLogger.info("Removing Row " + lastRow);
        details.remove(lastRow);
      }
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    finally
    {
      scanner.close();
    }
    return details;
  }
}
