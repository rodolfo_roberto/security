package ft.common.controller;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ft.SecurityInfo;

public class DatabaseController
{
  private Log FTLogger = LogFactory.getLog(DatabaseController.class);
  private final Date MIN_DATE = DateController.getFullDate12Hr("01/01/1970 12:00:00 am");
  private final Date MAX_DATE = DateController.getFullDate12Hr("31/12/9999 11:59:59 pm");
  private int connection_retry_count = 0;
  
  private Connection conn = null;
  
  private static DatabaseController thisInstance;
  
  private DatabaseController() {
	  
  }
  
  public static DatabaseController getInstance() {
	  
	  if (thisInstance == null) {
		  thisInstance = new DatabaseController();
	  }
	  
	  return thisInstance;
  }
  
  public int cleanUpSecurityInfo(Date date)
  {
    FTLogger.info("Security Information Cleanup Started");
    
    int affectedRow = 0;
    
    PreparedStatement prepStmt = null;
    ResultSet resultSet = null;

    try
    {
      conn = getConnection();
      
      prepStmt = conn.prepareStatement("UPDATE NGAC_USERINFO SET EmployeeNum =(?),AuthType =(?),Password =(?),RFID =(?),regDate =(?),expDate =(?) WHERE expDate <= (?)");
    
      prepStmt.setString(1, "");
      prepStmt.setString(2, "2");
      prepStmt.setString(3, "34B1BA3256C9B9EB8EAE66336F81A1A6");
      prepStmt.setString(4, "");
      prepStmt.setTimestamp(5, new Timestamp(MIN_DATE.getTime()));
      prepStmt.setTimestamp(6, new Timestamp(MAX_DATE.getTime()));
      prepStmt.setTimestamp(7, new Timestamp(date.getTime()));
    
      affectedRow = prepStmt.executeUpdate();
    }
    catch (Exception e)
    {
      FTLogger.error(e.toString());
      e.printStackTrace();
    }
    finally
    {
      closeQuery(resultSet, prepStmt);
    }
    FTLogger.info("Security Information Cleanup Completed");
    
    return affectedRow;
  }
  
  
  public Connection getConnection()
  {
    String driverName = Configuration.getString("DatabaseDriver");
    String connectStr = Configuration.getString("DatabaseURL");
    String uName = Configuration.getString("DatabaseUser");
    String pWord = Configuration.getString("DatabasePassword");
    String schema = Configuration.getString("DatabaseSchema");
    
    if (conn == null) {
      conn = createConnection(driverName, connectStr, uName, pWord, schema);
    }
    return conn;
  }
  
  @SuppressWarnings("resource")
public void updateSecurityInfo(SecurityInfo securityInfo)
  {
    boolean isExist = false;
    
    FTLogger.info("Security Information Update for : " + securityInfo.getStaffName());
    
    if (securityInfo.getStaffName().length() > 45) {
      FTLogger.warn("User name truncated: " + securityInfo.getStaffName());
      securityInfo.setStaffName(securityInfo.getStaffName().substring(0, 44));
    }
    
    Date createDate = DateController.getFullDate12Hr(securityInfo.getCreateDateStr());
    Date expiryDate = DateController.getFullDate12Hr(securityInfo.getExpiryDateStr());
    
    if (expiryDate == null) {
      expiryDate = DateController.getFullDate12Hr("31/12/9999 11:59:59 pm");
    }
    
    PreparedStatement prepStmt = null;
    ResultSet resultSet = null;
    Date regDate = null;
    
    if (securityInfo.getEmpNumber() != null && 
    		securityInfo.getEmpNumber().length() == 9) {
      
    	String firstTwo = securityInfo.getEmpNumber().substring(0, 2);
    	
        if (firstTwo.equals("99"))
        {
        	FTLogger.warn("User: " + securityInfo.getStaffName() + " updated id (starts with 99): " + securityInfo.getEmpNumber() );
        	securityInfo.setEmpNumber("3" + securityInfo.getEmpNumber().substring(2, 9));
        }
        
    }

    try
    {
      conn = getConnection();
      
      prepStmt = conn.prepareStatement("SELECT EmployeeNum, regDate FROM NGAC_USERINFO WHERE EmployeeNum = (?)");

      prepStmt.setString(1, securityInfo.getEmpNumber());
      prepStmt.executeQuery();
      
      resultSet = prepStmt.executeQuery();
      if (resultSet.next())
      {
        regDate = resultSet.getDate("regDate");
        isExist = true;
      }
      
      if (isExist)
      {
    	  
        FTLogger.info("Update User Info...");
        if (regDate.after(createDate))
        {
          FTLogger.error("Creation Date is older than the one in the existing record! Record not updated.");
          return;
        }
        
        prepStmt = conn.prepareStatement("UPDATE NGAC_USERINFO SET RFID = (?),Name = (?),Department = (?),regDate = (?),expDate = (?) WHERE EmployeeNum = (?)");
        
        prepStmt.setString(1, securityInfo.getCardNumber());
        prepStmt.setString(2, securityInfo.getStaffName());
        prepStmt.setString(3, securityInfo.getGroupName());
        prepStmt.setTimestamp(4, new Timestamp(createDate.getTime()));
        prepStmt.setTimestamp(5, new Timestamp(expiryDate.getTime()));
        prepStmt.setString(6, securityInfo.getEmpNumber());
        prepStmt.executeUpdate();
        
      }
      else
      {
        FTLogger.info("Adding User Info...");
        
        if (expiryDate.after(new Date()))
        {
          boolean isMaxRecord = true;
          
          prepStmt = conn.prepareStatement("SELECT ID FROM NGAC_USERINFO WHERE EmployeeNum = ''");
          resultSet = prepStmt.executeQuery();

          if (resultSet.next())
          {
            isMaxRecord = false;
            
            String recId = resultSet.getString("ID");
            prepStmt = conn.prepareStatement("UPDATE NGAC_USERINFO SET RFID = (?),Name = (?),EmployeeNum = (?),Department = (?),AuthType = 4,regDate = (?),expDate = (?) WHERE ID = (?)");

            prepStmt.setString(1, securityInfo.getCardNumber());
            prepStmt.setString(2, securityInfo.getStaffName());
            prepStmt.setString(3, securityInfo.getEmpNumber());
            prepStmt.setString(4, securityInfo.getGroupName());
            prepStmt.setTimestamp(5, new Timestamp(createDate.getTime()));
            prepStmt.setTimestamp(6, new Timestamp(expiryDate.getTime()));
            prepStmt.setString(7, recId);
            prepStmt.executeUpdate();
            
          }
          
          if (isMaxRecord)
          {
            FTLogger.error("Max Record Already Reached!!!");
            FTLogger.error("Please Add more slots or free up some records.");
          }
          
        }
        else
        {
          FTLogger.info("Card(" + securityInfo.getCardNumber() + ") is Already Expired!!!");
        }
      }
    }
    catch (Exception e)
    {
      FTLogger.error(e.toString());
      e.printStackTrace();
    }
    finally
    {
      closeQuery(resultSet, prepStmt);
    }
    
    FTLogger.info("Security Information Update for " + securityInfo.getStaffName() + " completed!");
  }
  
  public final void closeConnection()
  {
    if (conn != null) {
      try
      {
        conn.commit();
        conn.close();
        conn = null;
      }
      catch (SQLException e)
      {
        FTLogger.error("Exception during closeing statement");
        e.printStackTrace();
      }
    }
  }
  
  private final synchronized void closeQuery(ResultSet rs, Statement stmt)
  {
    closeResultSet(rs);
    closeStatement(stmt);
  }
  
  private final void closeResultSet(ResultSet rs)
  {
    if (rs != null) {
      try
      {
        rs.close();
        rs = null;
      }
      catch (SQLException e)
      {
        FTLogger.error("Exception when trying close result set");
        e.printStackTrace();
      }
    }
  }
  
  private final void closeStatement(Statement stmt)
  {
    if (stmt != null) {
      try
      {
        stmt.close();
        stmt = null;
      }
      catch (SQLException e)
      {
        FTLogger.error("Exception during closeing statement");
        e.printStackTrace();
      }
    }
  }
  
  private Connection createConnection(String driverName, String connectStr, String uName, String pWord, String schema)
  {
    Connection conn = null;
    
    Properties p = new Properties();
    p.setProperty("user", uName);
    p.setProperty("password", pWord);
    if ((schema != null) && (schema.trim().length() > 0)) {
      p.setProperty("currentSchema", schema);
    }
    try
    {
      if (conn == null)
      {
        Class.forName(driverName);
        conn = DriverManager.getConnection(connectStr, p);
      }
      if (conn == null) {
        throw new Exception();
      }
      connection_retry_count = 0;
    }
    catch (Exception e)
    {
      FTLogger.error("Error getting connection.");
      FTLogger.error(e.getMessage());
      connection_retry_count += 1;
      if (connection_retry_count <= 5)
      {
        FTLogger.error("Retrying...");
        try
        {
          Thread.sleep(5000L);
        }
        catch (InterruptedException ie)
        {
          ie.printStackTrace();
        }
        FTLogger.error("Retry Count : " + connection_retry_count);
        conn = createConnection(driverName, connectStr, uName, pWord, schema);
      }
    }
    return conn;
  }
}
