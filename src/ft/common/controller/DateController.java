/*  1:   */ package ft.common.controller;
/*  2:   */ 
/*  3:   */ import java.text.ParseException;
/*  4:   */ import java.text.SimpleDateFormat;
/*  5:   */ import java.util.Date;
/*  6:   */ 
/*  7:   */ public class DateController
/*  8:   */ {
/*  9: 8 */   private static SimpleDateFormat fullDate12Hr = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
/* 10:   */   public static final String MAX_DATE = "31/12/9999 11:59:59 pm";
/* 11:   */   public static final String MIN_DATE = "01/01/1970 12:00:00 am";
/* 12:   */   
/* 13:   */   public static Date getFullDate12Hr(String dateStr)
/* 14:   */   {
/* 15:15 */     Date date = null;
/* 16:   */     try
/* 17:   */     {
/* 18:18 */       date = fullDate12Hr.parse(dateStr);
/* 19:   */     }
/* 20:   */     catch (ParseException e)
/* 21:   */     {
/* 22:21 */       e.printStackTrace();
/* 23:   */     }
/* 24:23 */     return date;
/* 25:   */   }
/* 26:   */ }


/* Location:           D:\SGH\Security.jar
 * Qualified Name:     ft.common.controller.DateController
 * JD-Core Version:    0.7.0.1
 */