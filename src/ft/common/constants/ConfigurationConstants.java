package ft.common.constants;

public class ConfigurationConstants
{
  public static final String LOG_CONFIG = "log4j.properties";
  public static final String SKIPPED_ROW_COUNT = "skippedRowCnt";
  public static final String IGNORE_ROW_COUNT = "ignoreRowCnt";
  public static final String FILE_EXTENSION = "fileExt";
  public static final String SOURCE_DIRECTORY = "srcDir";
  public static final String PROCESSED_DIRECTORY = "processedDir";
  public static final String ERROR_DIRECTORY = "errorDir";
  public static final String FILE_PREFIX = "filePrefix";
  public static final String FIELD_DELIMITER = "delimiter";
  public static final String DB_URL = "DatabaseURL";
  public static final String DB_USER = "DatabaseUser";
  public static final String DB_PASS = "DatabasePassword";
  public static final String DB_DRIVER = "DatabaseDriver";
  public static final String DB_SCHEMA = "DatabaseSchema";
  public static final String IGNORE_GROUP = "IgnoreGroup";
}


/* Location:           D:\SGH\Security.jar
 * Qualified Name:     ft.common.constants.ConfigurationConstants
 * JD-Core Version:    0.7.0.1
 */